# **Aplicações Distribuídas - INF/UFG - 2017/2**

Uma breve explicação sobre o uso de tecnologias de Comunicação Indireta.
Teremos a seguir um resumido tutorial da tecnologia **Memcached**.

## **Comunicação Indireta e o conceito de Publisher/Subscriber**

É definida como a comunicação entre entidades de um sistema distribuído por meio de um intermediário, sem nenhum acoplamento direto entre o remetente e o destinatário(s).

O remetente não precisa saber a identidade do destinatário e vice-versa. Esse desacoplamento espacial permite que desenvolvedores tenham muitos graus de liberdade para lidar com alterações. Remetentes e destinatários não precisam existir ao mesmo tempo para se comunicar.

Nesse sentido, **Publisher Subscriber** consiste na transmissão de **mensagens assíncronas**. Para tanto, são criados mecanismos que possam transmitir notificações de eventos assíncronos aos endpoints que conectem ambos extremos para troca de dados (mensagens).

O componente chamado **publisher** envia uma mensagem para o tópico. Os tópicos de mensagem transmitem essas mensagens para todos os **subscribers**.


## **Memcached**

É um sistema de cache de objetos na memória de um Sistema Distribuído de alto desempenho. Originalmente destinado a ser usado para acelerar aplicativos web dinâmicos, aliviando a carga do banco de dados.

Funciona como uma memória de curto prazo para as aplicações, análogo à memoria cache do processador de um computador.

O memcached permite a realocação de memória de partes do seu sistema onde esta é mais abundante, para áreas onde há menos que o necessário. Permite assim que se faça um melhor uso dos recursos de seu sistema.

Podemos observar dois cenários de implantação na imagem abaixo:
<p>
	<img src="https://memcached.org/memcached-usage.png">
</p>

Cada nó é completamente independente (superior).
Cada nó pode fazer uso da memória de outros nós (parte inferior).

Na parte superior da figura, o primeiro cenário ilustra a estratégia de implantação clássica, onde há um desperdício, uma vez que, o tamanho total do cache é uma fração da capacidade real do que se pode utilizar, onde também é grande o esforço necessário para manter o cache consistente em todos esses nós.

Na parte inferior, ilustrando o memcached, percebe-se que todos os servidores estão olhando para o mesmo grupo virtual de memória. O que significa que um determinado item é sempre armazenado e sempre recuperado da mesma localização em todo o seu cluster web.

Além disso, à medida que a demanda pela aplicação cresce até um ponto em que se precisa ter mais servidores, cresce a quantidade de dados que devem ser acessados regularmente.

Não é necessário utilizar a memória do servidor web para o cache. Muitos usuários do memcached possuem máquinas dedicadas que são construídas para serem apenas servidores memcached.

#### Curiosidade ####
O Memcached foi originalmente desenvolvido por Brad Fitzpatrick para o LiveJournal em 2003.

## **Instalando no Windows**

O Memcached é originalmente um aplicativo linux, mas, como ele é de código aberto, também foi compilado para o Windows:

[http://downloads.northscale.com/memcached-win32-1.4.4-14.zip](http://downloads.northscale.com/memcached-win32-1.4.4-14.zip)
[http://downloads.northscale.com/memcached-win64-1.4.4-14.zip](http://downloads.northscale.com/memcached-win64-1.4.4-14.zip)
[http://downloads.northscale.com/memcached-1.4.5-x86.zip](http://downloads.northscale.com/memcached-1.4.5-x86.zip)
[http://downloads.northscale.com/memcached-1.4.5-amd64.zip](http://downloads.northscale.com/memcached-1.4.5-amd64.zip)

Em versões 1.4.4 e anteriores, o memcached podia ser instalado como um service. No entanto, a capacidade de executar o memcached como um serviço foi removida desde a versão 1.4.5. Portanto, as etapas de instalação estão divididas em duas categorias.

### ***Versão Service (<1.4.5)***

Extraia o arquivo binário do Windows memcached. Execute um prompt de comando com privilégios de administrador e digite:
```
c:\caminho\para\o\memcached\memcached.exe -d install
```

Para iniciar ou terminar o serviço memcached:
```
c:\memcached\memcached.exe -d start
c:\memcached\memcached.exe -d stop
```

Para alterar a configuração do memcached, execute regedit.exe e navegue até "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\memcached". Se desejar aumentar o limite de memória do memcached, edite o valor do ImagePath da seguinte forma:
```
"c:\caminho\para\o\memcached\memcached.exe" -d runservice -m 512
```

Outros parâmetros podem ser adicionados também, digite "c:\memcached\memcached.exe -h" para maiores informações de configuração do Memcached

### ***Versão sem Service (>=1.4.5)***

Na versão 1.4.5 ou posterior, o memcached não pode ser executado como um serviço. Ele deve ser iniciado como um processo normal usando o agendador de tarefas. Para configurar o processo memcached para ser executado automaticamente toda vez que o Windows for iniciado, execute um prompt de comando com privilégios elevados e digite:
```
schtasks /create /sc onstart /tn memcached /tr "'c:\caminho\para\o\memcached\memcached.exe' -m 512"
```

Para remover as tarefas memorizadas do memcached:
```
schtasks /delete /tn memcached
```

## **Referências**


* [JournalDev - Memcached Tutorial](https://www.journaldev.com/1117/memcached-tutorial)
* [Memcached - Site Oficial](https://memcached.org/about)
* [TutorialsPoint - Memcached Basics](https://www.tutorialspoint.com/memcached/)
* [Installing Memcached](https://commaster.net/content/installing-memcached-windows)


